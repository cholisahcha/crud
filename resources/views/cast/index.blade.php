@extends('adminLTE.master')

@section('content')

        <div class="mt-3 ml-3">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Menampilkan List Data Para Pemain Film </h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
              <!-- /Menampilkan alert success -->
                    @if(session('success'))
                        <div class="alert alert-success">
                            {{session ('success')}}
                        </div>
                    @endif
              <!-- /End-Menampilkan alert success -->
              <a class="btn btn-primary mb-2" href="/cast/create"> Create New Cast</a> <!--kembali ke link sebelumnya-->
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>NAMA</th>
                      <th>UMUR</th>
                      <th>BIODATA</th>
                      <th>Actions</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($cast as $key => $cast)
                        <tr>
                            <th> {{$key + 1}} </th>
                            <th> {{$cast->nama}} </th>
                            <td> {{$cast->umur}} </td>
                            <td> {{$cast->bio}} </td>
                            <td>
                                <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">show</a>
                                <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm">edit</a>
                                <form action="/cast/{{$cast->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="4" align="center"> No Data</td>
                            </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
@endsection