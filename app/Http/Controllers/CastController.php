<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    //cast-create Menampilkan Form utk membuat data pemain
    public function create(){
        return view('cast.create');
    }
    
    //store Menyimpan Data Baru ke tabel Cast
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast')->with('success', 'Cast berhasil disimpan!');
    }

    //index Menampilkan list data
    public function index() {
        $cast = DB::table('cast')->get(); //select*from post
        //dd($cast);
        return view('cast.index', compact('cast'));
    }

    //show Menampilkan data pemain dgn id tertentu
    public function show($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        //dd($cast);
        return view('cast.show', compact('cast'));
    }

    //edit
    public function edit($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        //dd($cast);
        return view('cast.edit', compact('cast'));
    }

    //update
    public function update ($cast_id, Request $request){
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
                    ->where('id', $cast_id)
                    ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio'  => $request['bio']
                    ]);
       return redirect('/cast')->with('success', 'Berhasil Update Cast!');
    }       

    //delete
    public function destroy($cast_id){
        $query =DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast')->with('success', 'Cast berhasil dihapus!');
    }
    
}
